
'use strict';

const slack = require('slack');
const _ = require('lodash');
const config = require('./config');
const msg = require('./msg');

let bot = slack.rtm.client();

bot.started((payload) => {
    this.self = payload.self
});

bot.message((message) => {
    if (!message.user) return;

    console.log(message);

    msg(message.text).then(function (res) {
        console.log(123132, res);
        slack.chat.postMessage({
            token: config('SLACK_TOKEN'),
            icon_emoji: config('ICON_EMOJI'),
            channel: message.channel,
            username: 'Starbot',
            text: res
        }, (err, data) => {
            if (err) throw err;

            let txt = _.truncate(data.message.text);

            console.log(`?  beep boop: I responded with "${txt}"`);
        })
    });
});

module.exports = bot;