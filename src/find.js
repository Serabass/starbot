var qa = require('./parse'),
    fs = require('fs-promise'),
    MyStem,
    myStem
    ;

try {
    MyStem = require('mystem3');
} catch (e) {
    MyStem = require('../node-mystem3')
}

myStem = new MyStem();

function lemmatizeString(str) {
    var x = str
        .replace(/[^А-Яа-яёЁ\d\s]/g, '')
        .split(/\s+/)
        .map(function (word) {
            return myStem.lemmatize(word);
        });

    return Promise.all(x);
}

function find(query) {
    myStem.start();

    return qa().then(function (list) {
        var questions = list
            .map(function (el) {
                return lemmatizeString(el.Q);
            });

        return Promise.all(questions).then(function (res) {
            return lemmatizeString(query)
                .then(function (queryLem) {

                    var data = res.map(function (w) {
                        return w.map(function (word) {
                            if (queryLem.indexOf(word) >= 0) {
                                return 1;
                            }

                            return 0;
                        });
                    }).map(function (x) {
                        return x.filter(b => b === 1);
                    });

                    var maxIndex = -1,
                        maxLength = 0;

                    data.forEach(function (row, i) {
                        if (row.length > maxLength) {
                            maxLength = row.length;
                            maxIndex = i;
                        }
                    });

                    // myStem.stop();

                    if (maxIndex === -1)
                        return ':(';

                    return list[maxIndex];
                });
        });
    })
        .catch(function (err) {
            console.error('caught', err);
            myStem.stop();
        });
}

module.exports = find;