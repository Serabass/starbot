
var GoogleSpreadsheet = require('google-spreadsheet');
var async = require('async');
var fs = require('fs-promise');
 
// spreadsheet key is the long id in the sheets URL 
var doc = new GoogleSpreadsheet('15AC6F28Tdd21gjfhg2miZtOsIaFe-xT8sVs4hB3g_6k');

function getData() {
	return new Promise(function (resolve, reject) {
		async.series([
		  function setAuth(step) {
			// see notes below for authentication instructions! 
			var creds = require('./Node-8d8e43ba2abc.json');
			doc.useServiceAccountAuth(creds, step);
		  },
		  function getInfoAndWorksheets(step) {
			doc.getInfo(function(err, info) {
				
				if (err)
					throw err;
				
			  sheet = info.worksheets[0];
			  step();
			});
		  },
		  function workingWithRows() {
			// google provides some query options 
			sheet.getRows({
			  offset: 1
			}, function( err, rows ) {
				if (err)
					throw err;
				
				resolve(rows);
			});
		  }
		]);
	});
	
}

function update() {
	return getData().then(function (rows) {
		var result = rows.map(function (row) {
			return {
				num: row.num,
				N: row.name,
				Q: row.question,
				C: row.comment,
				A: row.answer
			};
		});

		return fs.writeFile(__dirname + '/QA.json', JSON.stringify(result, null, 4));
	})
	.catch(function (e) {
		throw e;
	});
}

module.exports = update;