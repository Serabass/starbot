
'use strict';

const slack = require('slack');
const _ = require('lodash');
const config = require('./config');

var find = require('./find');
var qa = require('./parse');
var gUpdate = require('./gdocs-update');


function stdAnswer(text, res) {
  return 'Ответ на: ' + text + '\n'
      + 'Q:\n'
      + res.Q + '\n'
      + '\n'
      + 'A:\n'
      + res.A + '\n'
      + '\n';
}

function msg(text) {
  console.log('Q: ' + text);
  return new Promise(function (resolve, reject) {
    var m;
    if (text === '!upd') {
      return gUpdate().then(function () {
        resolve('ok');
      })
          .catch(function (err) {
            resolve('err' + JSON.stringify(err));
          });
    } else if (m = text.match(/^[№#](\d+)/)) {
      qa().then(function (list) {
        var res = list.find(function (el) {
          return el.num == m[1];
        });

        if ( ! res) {
          resolve(':(');
        } else {
          resolve(stdAnswer(text, res));
        }
      });
    } else {
      return find(text)
          .then(function (res) {
            console.log('find', res);
            if (res === null) {
              resolve(':(');
            } else {
              resolve(stdAnswer(text, res));
            }
          }).catch(function (err) {
            console.log('find-err', err);
          });
    }
  });
}


let bot = slack.rtm.client();

bot.started((payload) => {
  this.self = payload.self
});

bot.message((msg) => {
  if (!msg.user) return;
  if (!_.includes(msg.text.match(/<@([A-Z0-9])+>/igm), `<@${this.self.id}>`)) return;


  msg(message.text).then(function (res) {
    console.log(123132, res);
    slack.chat.postMessage({
      token: config('SLACK_TOKEN'),
      icon_emoji: config('ICON_EMOJI'),
      channel: msg.channel,
      username: 'Starbot',
      text: res
    }, (err, data) => {
      if (err) throw err;

      let txt = _.truncate(data.message.text);

      console.log(`🤖  beep boop: I responded with "${txt}"`);
    })
  });
});

bot.run();