
var gUpdate = require('./gdocs-update');
var qa = require('./parse');
var find = require('./find');

function stdAnswer(text, res) {
    return 'Ответ на: ' + text + '\n'
        + 'Q:\n'
        + res.Q + '\n'
        + '\n'
        + 'A:\n'
        + res.A + '\n'
        + '\n';
}

function msg(text) {
    console.log('Q: ' + text);
    return new Promise(function (resolve, reject) {
        var m;
        if (text === '!upd') {
            return gUpdate().then(function () {
                resolve('ok');
            })
                .catch(function (err) {
                    resolve('err' + JSON.stringify(err));
                });
        } else if (m = text.match(/^[№#](\d+)/)) {
            qa().then(function (list) {
                var res = list.find(function (el) {
                    return el.num == m[1];
                });

                if ( ! res ) {
                    resolve(':(');
                } else {
                    resolve(stdAnswer(text, res));
                }
            });
        } else {
            console.log('step 1');
            return find(text)
                .then(function (res) {
                    console.log('find', res);
                    if (res === null || res === ':(') {
                        resolve(':(');
                    } else {
                        resolve(stdAnswer(text, res));
                    }
                }).catch(function (err) {
                    console.log('find-err', err);
                });
        }
    });
}

module.exports = msg;