var fs = require('fs-promise');

function parseQA() {
    return new Promise(function (resolve, reject) {
        fs
            .readFile(__dirname + '/QA.json')
            .then(function (data) {
                resolve(JSON.parse(data.toString()));
            }).catch(function (err) {
                console.error('parse-err', err);
            });
    });
}

module.exports = parseQA;